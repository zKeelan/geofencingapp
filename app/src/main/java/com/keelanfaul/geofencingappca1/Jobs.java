package com.keelanfaul.geofencingappca1;

public class Jobs {
    private String job_id;
    private String job_name;
    private String latitude;
    private String longitude;
    private String location_name;

    public Jobs() {
    }

    public Jobs(String job_id, String job_name, String latitude, String longitude, String location_name) {
        this.job_id = job_id;
        this.job_name = job_name;
        this.latitude = latitude;
        this.longitude = longitude;
        this.location_name = location_name;
    }

    public String getJob_id() {
        return job_id;
    }

    public void setJob_id(String job_id) {
        this.job_id = job_id;
    }

    public String getJob_name() {
        return job_name;
    }

    public void setJob_name(String job_name) {
        this.job_name = job_name;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLocation_name() {
        return location_name;
    }

    public void setLocation_name(String location_name) {
        this.location_name = location_name;
    }
}
