package com.keelanfaul.geofencingappca1;

public class Rota {
    private String clockInTime;
    private String clockOutTime;
    private String job_id;
    private String user_id;

    public Rota() {
    }

    public Rota(String clockInTime, String clockOutTime, String job_id, String user_id) {
        this.clockInTime = clockInTime;
        this.clockOutTime = clockOutTime;
        this.job_id = job_id;
        this.user_id = user_id;
    }

    public String getClockInTime() {
        return clockInTime;
    }

    public void setClockInTime(String clockInTime) {
        this.clockInTime = clockInTime;
    }

    public String getClockOutTime() {
        return clockOutTime;
    }

    public void setClockOutTime(String clockOutTime) {
        this.clockOutTime = clockOutTime;
    }

    public String getJob_id() {
        return job_id;
    }

    public void setJob_id(String job_id) {
        this.job_id = job_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }


}
