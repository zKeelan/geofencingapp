package com.keelanfaul.geofencingappca1;

public class User {
    private String user_id;
    private String firstName;
    private String lastName;
    private double payrate;

    public User() {
    }

    public User(String user_id, String firstName, String lastName, double payrate) {
        this.user_id = user_id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.payrate = payrate;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public double getPayrate() {
        return payrate;
    }

    public void setPayrate(double payrate) {
        this.payrate = payrate;
    }
}
