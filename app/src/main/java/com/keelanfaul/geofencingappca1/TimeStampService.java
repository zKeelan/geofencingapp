package com.keelanfaul.geofencingappca1;
import java.util.Calendar;
import java.util.Date;
import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;

public class TimeStampService extends Service {
    private final IBinder binder = new LocalBinder();
    public class LocalBinder extends Binder {
        TimeStampService getService() {
            return TimeStampService.this;
        }
    }
    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }
    public Date getCurrentDate() {
        return Calendar.getInstance().getTime();
    }
}
