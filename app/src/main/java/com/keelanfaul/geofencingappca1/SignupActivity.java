package com.keelanfaul.geofencingappca1;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

public class SignupActivity extends AppCompatActivity {
    private EditText userEmail;
    private EditText userFirstName;
    private EditText userSurname;
    private EditText userPassword;
    private Button signupButton;
    private FirebaseAuth mAuth;
    private FirebaseFirestore db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        userEmail = findViewById(R.id.signup_email);
        userFirstName = findViewById(R.id.signup_firstname);
        userSurname = findViewById(R.id.signup_surname);
        userPassword = findViewById(R.id.signup_password);
        signupButton = findViewById(R.id.signup_btn);
        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();




        signupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final ProgressDialog progressDialog = new ProgressDialog(SignupActivity.this);
                progressDialog.setMessage("Registering, Please wait...");
                progressDialog.show();

                String email = userEmail.getText().toString();
                String password = userPassword.getText().toString();

                mAuth.createUserWithEmailAndPassword(email, password)
                        .addOnCompleteListener(SignupActivity.this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                Log.d("FireBaseSuccess","createUser:onComplete:" + task.isSuccessful());
                                if (task.isSuccessful()) {
                                    FirebaseUser user = mAuth.getCurrentUser();
                                    String user_id = user.getUid().toString();
                                    User newUser = new User(user_id,userFirstName.getText().toString(),userSurname.getText().toString(),9.85);
                                    DocumentReference newUserRef = db.collection("USERS").document();
                                    newUserRef.set(newUser).addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            progressDialog.dismiss();
                                            if (task.isSuccessful()){
                                                Intent intent;
                                                intent = new Intent(SignupActivity.this, LoginActivity.class);
                                                startActivity(intent);
                                            }else{
                                                Toast.makeText(SignupActivity.this, "Sign Up Failed",
                                                        Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    });
                                } else {
                                    Toast.makeText(SignupActivity.this, "Sign Up Failed",
                                            Toast.LENGTH_SHORT).show();
                                }
                            }
                        });

            }
        });

    }
}
