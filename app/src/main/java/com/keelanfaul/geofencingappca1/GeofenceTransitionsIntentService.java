package com.keelanfaul.geofencingappca1;

import android.app.IntentService;
import android.content.Intent;
import android.content.Intent;
import android.util.Log;


import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingEvent;


public class GeofenceTransitionsIntentService extends IntentService {

    private static final String TAG = "GeofenceTransitions";
    Intent broadcastIntent = new Intent("GeoFence");



    public GeofenceTransitionsIntentService() {
        super("GeofenceTransitionsIntentService");
    }




    @Override
    protected void onHandleIntent(Intent intent) {

        Log.i(TAG, "onHandleIntent");

        GeofencingEvent geofencingEvent = GeofencingEvent.fromIntent(intent);
        if (geofencingEvent.hasError()) {
            Log.e(TAG, "Goefencing Error " + geofencingEvent.getErrorCode());
            return;
        }

        int geofenceTransition = geofencingEvent.getGeofenceTransition();

        Log.i(TAG, "geofenceTransition = " + geofenceTransition + " Enter : " + Geofence.GEOFENCE_TRANSITION_ENTER + "Exit : " + Geofence.GEOFENCE_TRANSITION_EXIT);
        if (geofenceTransition == Geofence.GEOFENCE_TRANSITION_ENTER || geofenceTransition == Geofence.GEOFENCE_TRANSITION_DWELL){
            broadcastIntent.putExtra("Enter", true);
            sendBroadcast(broadcastIntent);
        }
        else if(geofenceTransition == Geofence.GEOFENCE_TRANSITION_EXIT) {
            Log.i(TAG, "Showing Notification...");
            broadcastIntent.putExtra("Exit", true);
            sendBroadcast(broadcastIntent);

        } else {
            // Log the error.

            Log.e(TAG, "Error ");
        }
    }

}
