package com.keelanfaul.geofencingappca1;

import android.Manifest;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.circularreveal.CircularRevealWidget;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingClient;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import java.util.Calendar;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class MapActivity extends AppCompatActivity implements OnMapReadyCallback {

    @Override
    public void onMapReady(GoogleMap googleMap) {
        Log.d(TAG, "onMapReady: map is ready");
        mMap = googleMap;
        if (mLocationPermissionsGranted) {
            getDeviceLocation();
            getJobs();


            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED){
                return;
            }
            mMap.setMyLocationEnabled(true);
            mGeofencingClient.addGeofences(getGeofencingRequest(), getGeofencePendingIntent())
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            Log.d(TAG, "Geofencing : Geofence added to map");

                        }
                    });


        }
    }

    private static final String TAG = "MapActivity";

    private static final String FINE_LOCATION = Manifest.permission.ACCESS_FINE_LOCATION;
    private static final String COURSE_LOCATION = Manifest.permission.ACCESS_COARSE_LOCATION;
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1234;
    private static final float DEFAULT_ZOOM = 15f;


    private FirebaseFirestore db;
    private Boolean mLocationPermissionsGranted = false;
    private FusedLocationProviderClient mFusedLocationProviderClient;
    private GoogleMap mMap;
    private TextView logoutBtn;
    private FirebaseAuth mAuth;
    private GeofencingClient mGeofencingClient;
    private List<Jobs> jobsList;
    private List<Geofence> mGeofenceList;
    private PendingIntent mGeofencePendingIntent;
    private String myLat;
    private String myLong;
    private CircleOptions circleOptions;
    private TextView clockInBtn;
    private TextView clockOutBtn;
    private TextView mapTextView1;
    TimeStampService TimeStampService;
    private boolean isBound = false;





    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        getLocationPermission();
        mGeofencingClient = LocationServices.getGeofencingClient(this);
        logoutBtn = findViewById(R.id.logoutBtn);
        clockInBtn = findViewById(R.id.clockInBtn);
        clockOutBtn = findViewById(R.id.clockOutBtn);
        mapTextView1 = findViewById(R.id.maptTextView1);
        mAuth = FirebaseAuth.getInstance();
        mGeofenceList = new ArrayList<Geofence>();
        db = FirebaseFirestore.getInstance();
        registerReceiver(Receiver, new IntentFilter("GeoFence"));





        logoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAuth.signOut();
                finish();
                Intent intent = new Intent(MapActivity.this, LoginActivity.class);
                startActivity(intent);
            }
        });

    }
    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = new Intent(this, TimeStampService.class);
        bindService(intent, connection, Context.BIND_AUTO_CREATE);
    }
    @Override
    protected void onStop() {
        super.onStop();
        if (isBound) {
            unbindService(connection);
            isBound = false;
        }
    }
    private ServiceConnection connection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            TimeStampService.LocalBinder binder = (TimeStampService.LocalBinder) service;
            TimeStampService = binder.getService();
            isBound = true;
        }
        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            isBound = false;
        }
    };

    public void startService(View v)
    {
        Intent serviceIntent = new Intent(this, AppService.class);

        ContextCompat.startForegroundService(this, serviceIntent);
    }

    public void stopService(View v) {
        Intent serviceIntent = new Intent(this, AppService.class);
        stopService(serviceIntent);

    }

        private void getDeviceLocation(){
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        try{
            if (mLocationPermissionsGranted){

                final Task location = mFusedLocationProviderClient.getLastLocation();
                location.addOnCompleteListener(new OnCompleteListener() {
                    @Override
                    public void onComplete(@NonNull Task task) {
                        if (task.isSuccessful()){
                         //   Toast.makeText(MapActivity.this, "Location found",Toast.LENGTH_SHORT).show();
                            Location currentLocation = (Location) task.getResult();
                            myLat = String.valueOf(currentLocation.getLatitude());
                            myLong = String.valueOf(currentLocation.getLongitude());
                            moveCamera(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()),DEFAULT_ZOOM);
                        }else {
                            Toast.makeText(MapActivity.this, "Current Location is Null",Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }

        } catch (SecurityException e){
            Log.e("DeviceLocation", "Security Exception" + e.getMessage());
        }
    }
    private void moveCamera(LatLng latLng, float zoom){
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));


    }


    private void initMap(){
        Log.d(TAG, "initMap: initializing map");
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);

        mapFragment.getMapAsync(MapActivity.this);
    }
    private void getJobs(){
        List<Jobs> jobsList = new ArrayList<>();
        jobsList.add(new Jobs(UUID.randomUUID().toString(), "Electrics & Plumbing","53.981644", "-6.392085", "DKIT Building Site"));
        jobsList.add(new Jobs(UUID.randomUUID().toString(), "Bits","54.001991", "-6.626262", "My house"));
        jobsList.add(new Jobs(UUID.randomUUID().toString(), "Electrics","54.001152", "-6.398763", "Marshes Dundalk Building Site"));
        jobsList.add(new Jobs(UUID.randomUUID().toString(), "Plumbing","54.006839", "-6.404524", "Long Walk Dundalk Building Site"));
        createGeofences(jobsList);

    }

    private void getLocationPermission(){
        Log.d(TAG, "getLocationPermission: getting location permissions");
        String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION};

        if(ContextCompat.checkSelfPermission(this.getApplicationContext(),
                FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){
            if(ContextCompat.checkSelfPermission(this.getApplicationContext(),
                    COURSE_LOCATION) == PackageManager.PERMISSION_GRANTED){
                mLocationPermissionsGranted = true;
                initMap();
            }else{
                ActivityCompat.requestPermissions(this,
                        permissions,
                        LOCATION_PERMISSION_REQUEST_CODE);
            }
        }else{
            ActivityCompat.requestPermissions(this,
                    permissions,
                    LOCATION_PERMISSION_REQUEST_CODE);
        }
    }

    public void createGeofences(List<Jobs> jobsList) {
        int amountOfJobs = jobsList.size();
        for (int i = 0; i < amountOfJobs; i++){
            MarkerOptions places = new MarkerOptions()
                    .position(new LatLng(Double.parseDouble(jobsList.get(i).getLatitude()), Double.parseDouble(jobsList.get(i).getLongitude())))
                    .title(jobsList.get(i).getLocation_name());
            mMap.addMarker(places);
            Geofence fence = new Geofence.Builder()
                    .setRequestId(jobsList.get(i).getJob_id())
                    .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER | Geofence.GEOFENCE_TRANSITION_EXIT)
                    .setCircularRegion(Double.parseDouble(jobsList.get(i).getLatitude()), Double.parseDouble(jobsList.get(i).getLongitude()), 150)
                    .setExpirationDuration(Geofence.NEVER_EXPIRE)
                    .build();
            CircleOptions circleOptions = new CircleOptions()
                    .strokeColor(Color.RED) //Outer black border
                    .fillColor(0x88ff0000) //inside of the geofence will be transparent, change to whatever color you prefer like 0x88ff0000 for mid-transparent red
                    .center(new LatLng(Double.parseDouble(jobsList.get(i).getLatitude()), Double.parseDouble(jobsList.get(i).getLongitude()))) // the LatLng Object of your geofence location
                    .radius(150);
            mMap.addCircle(circleOptions);
            mGeofenceList.add(fence);
            Log.d(TAG, "Geofence added to List ; id : " + jobsList.get(i).getJob_id() + ", lat : " +  jobsList.get(i).getLatitude() + ", long : " + jobsList.get(i).getLongitude());
        }



    }
    private GeofencingRequest getGeofencingRequest() {
        Log.d(TAG, "Geofence getGeofencing request;");
        GeofencingRequest.Builder builder = new GeofencingRequest.Builder();
        builder.setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER);
        builder.addGeofences(mGeofenceList);
        return builder.build();
    }
    private PendingIntent getGeofencePendingIntent(){
        Intent intent = new Intent(getApplicationContext(), GeofenceTransitionsIntentService.class);
        mGeofencePendingIntent = PendingIntent.getService(getApplicationContext(), 0, intent, PendingIntent.
                FLAG_UPDATE_CURRENT);
        return mGeofencePendingIntent;
    }

    private BroadcastReceiver Receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(final Context context, Intent intent) {
            try {
                if (intent.getAction().equals("GeoFence")) {
                    boolean enter = intent.getBooleanExtra("Enter", false);
                    boolean exit = intent.getBooleanExtra("Exit", false);
                    if (enter) {
                        Toast.makeText(context, "You entered location", Toast.LENGTH_SHORT).show();
                        mapTextView1.setVisibility(View.INVISIBLE);
                        clockInBtn.setVisibility(View.VISIBLE);
                        clockInBtn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(final View v) {
                                if (isBound) {
                                    Date date = TimeStampService.getCurrentDate();
                                    Toast.makeText(context, String.valueOf(date), Toast.LENGTH_SHORT).show();
                                }
                                startService(v);
                                clockInBtn.setVisibility(View.INVISIBLE);
                                clockOutBtn.setVisibility(View.VISIBLE);
                            }
                        });
                        clockOutBtn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(final View v) {
                                    clockInBtn.setVisibility(View.VISIBLE);
                                    clockOutBtn.setVisibility(View.INVISIBLE);
                                      stopService(v);

                                 Toast.makeText(MapActivity.this, "You Clocked out", Toast.LENGTH_SHORT).show();
//                                final ProgressDialog progressDialog = new ProgressDialog(MapActivity.this);
//                                progressDialog.setMessage("Clocking In.....");
//                                progressDialog.show();
//                                Date currentTime = Calendar.getInstance().getTime();
//                                Rota newRota = new Rota(currentTime.toString(), "", jobsList.get(1).getJob_id(), mAuth.getUid().toString());
//                                CollectionReference newRotaRef = db.collection("ROTA");
//                                newRotaRef.whereEqualTo("job_id",jobsList.get(1).getJob_id());
//                                newRotaRef.whereEqualTo("user_id", mAuth.getUid().toString());

                            }
                        });

                        clockOutBtn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (isBound) {
                                    Date date = TimeStampService.getCurrentDate();
                                    Toast.makeText(context, String.valueOf(date), Toast.LENGTH_SHORT).show();
                                }
                                clockInBtn.setVisibility(View.VISIBLE);
                                clockOutBtn.setVisibility(View.INVISIBLE);
                                stopService(v);

                            }
                        });
                    } else if (exit) {
                        Toast.makeText(context, "Leaving from your location", Toast.LENGTH_SHORT).show();
                        clockInBtn.setVisibility(View.INVISIBLE);
                        clockOutBtn.setVisibility(View.INVISIBLE);
                        mapTextView1.setVisibility(View.VISIBLE);

                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.d(TAG, "onRequestPermissionsResult: called.");
        mLocationPermissionsGranted = false;

        switch(requestCode){
            case LOCATION_PERMISSION_REQUEST_CODE:{
                if(grantResults.length > 0){
                    for(int i = 0; i < grantResults.length; i++){
                        if(grantResults[i] != PackageManager.PERMISSION_GRANTED){
                            mLocationPermissionsGranted = false;
                            Log.d(TAG, "onRequestPermissionsResult: permission failed");
                            return;
                        }
                    }
                    Log.d(TAG, "onRequestPermissionsResult: permission granted");
                    mLocationPermissionsGranted = true;
                    //initialize our map
                    initMap();
                }
            }
        }
    }


}



